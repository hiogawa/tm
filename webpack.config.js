const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const PRODUCTION = process.env.BUILD_ENV == 'production';
const outputPath = path.resolve(__dirname, 'out');

module.exports = {
    mode: (PRODUCTION ? 'production' : 'development'),
    entry: { index: './src/index.js' },
    output: {
        filename: (PRODUCTION ? '[name].[hash].js' : '[name].js'),
        path: outputPath,
    },
    plugins: [
        new HtmlWebpackPlugin({ template: 'src/index.html' }),
        new webpack.HotModuleReplacementPlugin(),
    ],
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                include: [ path.join(__dirname, 'src') ],
                loader: 'eslint-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [ path.join(__dirname, 'src') ],
                options: {
                    cacheDirectory: true,
                    presets: ['@babel/preset-react'],
                    plugins: ['react-hot-loader/babel']
                },
            },
            {
                test: /\.scss$/,
                use: [ 'style-loader', 'css-loader', 'sass-loader' ]
            }
        ]
    },
    devtool: (PRODUCTION ? 'source-map' : 'inline-source-map'),
    devServer: {
        contentBase: outputPath,
        historyApiFallback: true,
        hot: true,
    },
};
