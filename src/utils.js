import _ from 'lodash';
import OP from 'object-path-immutable';
let { floor, sqrt } = Math;

export const isBetweenV = ([v1, v2], v) =>
    _.zip(v1, v2, v).every(([x1, x2, x]) => isBetween([x1, x2], x));

export const plusV = (v1, v2) =>
    _.zip(v1, v2).map(([x1, x2]) => x1 + x2);

export const negV = (v1) => v1.map(x => -x);

export const subV = (v1, v2) => plusV(v1, negV(v2));

export const mulV = (v, s) => v.map(x => x * s);

export const normV = ([x, y]) => sqrt(x * x + y * y);

export const isBetween = ([x1, x2], x) => {
    if (x1 > x2) {
        [x1, x2] = [x2, x1];
    }
    return x1 <= x && x <= x2
};

// step > 0
export const mod = (value, step, start = 0) => {
    value = value - start;
    if (value < 0) {
        value = value + step * (floor((-value) / step) + 1);
    } else {
        value = value - step * floor((value) / step);
    }
    return value + start;
};

export const linterp = ([x1, y1], [x2, y2], t = 0.5) =>
    [[x1, x2], [y1, y2]].map(([p, q]) => (1 - t) * p + t * q);

export const px = n => `${n}px`;

export const cssXY = ([x, y]) => ({
    left: px(x), top: px(y)
});

export const cssWH = ([w, h]) => ({
    width: px(w), height: px(h)
});

export const assignPositions = (edge, nodes) => {
    let { source, target } = edge;
    let [ sourcePosition, targetPosition ] = [source, target].map(id => nodes.find(_.matches({ id })).position);
    return OP.assign(edge, '', { sourcePosition, targetPosition });
};

// Object path lambda (convienient as an argument for Component#setState)
/* Usage:
OPL.push('nodes', newNode)
OPL(['push', 'edges', newEdge],
    ['set', 'newEdgeStart', null])
this.setState(OPL(['push', 'edges', newEdge], ['set', 'newEdgeStart', null]))
*/
export const OPL = (...commands) =>
    _.flow(commands.map(([method, ...args]) => OPL[method](...args)));

['set', 'push', 'del', 'assign'].forEach(method => {
    OPL[method] = (...args) => obj => OP[method](obj, ...args);
});
