import React, { Component, createRef } from 'react';
import _ from 'lodash';
import CN from 'classnames';
import Node from './Node.js';
import Edge from './Edge.js';
import { plusV, assignPositions, linterp, OPL } from './utils.js';

const initialState = {
    mode: 'INITIAL', // 'ADD_NODE', 'MOVE_NODE', 'ADD_EDGE', 'MOVE_EDGE'
    cursorPosition: null,
    newEdgeSource: null,
    moveNodeId: null,
    moveEdgeId: null,
    nodes: [],
    edges: [],
};

class GraphEditor extends Component {
    constructor(props) {
        super(props);
        this.state = _.extend({}, initialState, props.graph);
        this.fileOutputRef = createRef();
        this.fileInputRef = createRef();
    }
    onSaveFile() {
        let { nodes, edges } = this.state;
        let file = new File([JSON.stringify({ nodes, edges }, null, 2)], { type : 'application/json' });
        let url = window.URL.createObjectURL(file);
        let aNode = this.fileOutputRef.current;
        aNode.href = url;
        aNode.download = 'graph.json';
        aNode.click();
        window.URL.revokeObjectURL(url);
    }
    onLoadFile() {
        let file = this.fileInputRef.current.files[0];
        let reader = new FileReader;
        reader.onload = (e) => {
            try {
                let graph = JSON.parse(e.target.result);
                this.setState(_.extend({}, initialState, graph));
            } catch (err) {
                window.alert('File cannot be loaded. Error Message:', err);
            }
        };
        reader.readAsText(file);
    }
    renderNav() {
        let { mode } = this.state;
        return (
            <div className='nav'>
                <button onClick={() => this.onSaveFile()}>
                    Save to File
                </button>
                <a ref={this.fileOutputRef} download />
                <button onClick={() => this.fileInputRef.current.click()}>
                    Load from File
                </button>
                <input type='file' ref={this.fileInputRef} onChange={() => this.onLoadFile()} />
                <button className={CN({ enabled: mode == 'ADD_NODE' })}
                    onClick={() =>
                        this.setState({ mode: mode == 'ADD_NODE' ? 'INITIAL' : 'ADD_NODE' })}
                >
                    Add Node
                </button>
                <button className={CN({ enabled: mode == 'ADD_EDGE' })}
                    onClick={() =>
                        this.setState({ mode: mode == 'ADD_EDGE' ? 'INITIAL' : 'ADD_EDGE' })}
                >
                    Add Edge
                </button>
                <button className={CN({ enabled: mode == 'MOVE_NODE' })}
                    onClick={() =>
                        this.setState({ mode: mode == 'MOVE_NODE' ? 'INITIAL' : 'MOVE_NODE' })}
                >
                    Move Node
                </button>
                <button className={CN({ enabled: mode == 'MOVE_EDGE' })}
                    onClick={() =>
                        this.setState({ mode: mode == 'MOVE_EDGE' ? 'INITIAL' : 'MOVE_EDGE' })}
                >
                    Move Edge
                </button>
            </div>
        );
    }
    render() {
        let { mode, cursorPosition, nodes, edges, newEdgeSource, moveNodeId, moveEdgeId } = this.state;
        edges = edges.map(e => assignPositions(e, nodes));
        let newEdgeSourceNode = nodes.find(_.matches({ id: newEdgeSource }));

        return (
            <div className='graph-editor'>
                { this.renderNav() }
                <div className='playground'
                    onMouseMove={(e) => {
                        let [x, y] = [e.clientX, e.clientY];
                        let { offsetLeft: x1, offsetWidth: dx, offsetTop: y1, offsetHeight: dy } = e.currentTarget;
                        if (x1 <= x && x <= x1 + dx && y1 <= y && y <= y1 + dy) {
                            let newCursorPosition = [ x - x1, y - y1 ];
                            this.setState({ cursorPosition: newCursorPosition });
                            if (mode == 'MOVE_NODE' && moveNodeId) {
                                let i = nodes.findIndex(_.matches({ id: moveNodeId }));
                                this.setState(OPL.set(`nodes.${i}.position`, newCursorPosition));
                            }
                            if (mode == 'MOVE_EDGE' && moveEdgeId) {
                                let i = edges.findIndex(_.matches({ id: moveEdgeId }));
                                this.setState(OPL.set(`edges.${i}.interPosition`, newCursorPosition));
                            }
                        } else {
                            this.setState({ cursorPosition: null });
                        }
                    }}
                >
                    { nodes.map(props =>
                        <Node key={props.id}
                            {...props}
                            moving={ props.id == moveNodeId }
                            onClick={() => {
                                if (mode == 'ADD_EDGE') {
                                    this.setState({ newEdgeSource: props.id })
                                }
                                if (mode == 'MOVE_NODE') {
                                    this.setState({ moveNodeId: moveNodeId ? null : props.id })
                                }
                                if (mode == 'ADD_EDGE' && newEdgeSourceNode) {
                                    let interPosition;
                                    if (newEdgeSource == props.id) {
                                        interPosition = plusV(props.position, [30, 30]);
                                    } else {
                                        interPosition = linterp(newEdgeSourceNode.position, props.position);
                                    }
                                    let newEdge = {
                                        id: Math.floor(Math.random() * 1000),
                                        source: newEdgeSourceNode.id,
                                        target: props.id,
                                        interPosition,
                                    };
                                    this.setState(OPL(
                                        ['push', 'edges', newEdge],
                                        ['set', 'newEdgeSource', null]));
                                }
                            }}
                            onDoubleClick={(e) => {
                                if (mode == 'INITIAL') {
                                    let label = window.prompt('Edit node label', props.label);
                                    if (label) {
                                        let i = nodes.findIndex(_.matches({ id: props.id }));
                                        this.setState(OPL.set(`nodes.${i}.label`, label));
                                    }
                                }
                                e.stopPropagation();
                            }} />)}
                    { mode == 'ADD_NODE' && cursorPosition &&
                        <Node isNew={true}
                            position={cursorPosition}
                            onClick={() => {
                                let newNode = {
                                    id: Math.floor(Math.random() * 1000),
                                    position: cursorPosition,
                                }
                                this.setState(OPL.push('nodes', newNode));
                            }} />}
                    { edges.map(props =>
                        <Edge key={props.id}
                            {...props}
                            moving={ props.id == moveEdgeId }
                            onClick={() => {
                                if (mode == 'MOVE_EDGE') {
                                    this.setState({ moveEdgeId: moveEdgeId ? null : props.id });
                                }
                            }}
                            onDoubleClick={(e) => {
                                if (mode == 'INITIAL') {
                                    let label = window.prompt('Edit edge label', props.label);
                                    if (label) {
                                        let i = edges.findIndex(_.matches({ id: props.id }));
                                        this.setState(OPL.set(`edges.${i}.label`, label));
                                    }
                                }
                                e.stopPropagation();
                            }} />)}
                    { mode == 'ADD_EDGE' && cursorPosition && newEdgeSourceNode &&
                        <Edge isNew={true}
                            sourcePosition={newEdgeSourceNode.position}
                            targetPosition={cursorPosition}
                            interPosition={linterp(newEdgeSourceNode.position, cursorPosition)} />}
                </div>
            </div>
        );
    }
}

export default GraphEditor;
