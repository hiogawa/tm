import React from 'react';
import CN from 'classnames';
import { cssXY } from './utils.js';

const Node = ({ position, label, moving, isNew, onClick, onDoubleClick }) => {
    return (
        <div className={CN('node', { 'node--moving': moving, 'node--new': isNew })}
            style={cssXY(position)}
            {...{ onClick, onDoubleClick }}
        >
            { label && <div className='node__tooltip'>{ label }</div> }
        </div>
    );
};

export default Node;
