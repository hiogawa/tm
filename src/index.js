import './scss/index.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import GraphEditor from './GraphEditor.js';
import { linterp } from './utils.js';

const nodes = _.range(6).map(i => ({
    id: i + 1,
    position: [_.random(10, 600), _.random(10, 600)],
    label: `Node ${i + 1}`
}));
const edges = _.flatten(nodes.map(n1 => nodes.map(n2 => [n1, n2])))
                .filter(() => _.random(1, true) <= 0.2)
                .filter(([n1, n2]) => n1.id != n2.id)
                .map(([n1, n2], j) => ({
    id: j + 1,
    source: n1.id,
    target: n2.id,
    interPosition: linterp(n1.position, n2.position, _.random(0.1, 0.9)),
    label: `Edge ${n1.id} to ${n2.id}`
}));
const graph = { nodes, edges };

ReactDOM.render(<GraphEditor graph={graph} />, document.getElementById('app'));
