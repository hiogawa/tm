import React from 'react';
import _ from 'lodash';
import CN from 'classnames';
import { isBetweenV, plusV, subV, mulV, normV, mod, cssXY, cssWH } from './utils.js';
const { min, max, atan2, PI } = Math;

const EdgePart = ({ startPosition, endPosition, clockwise, className }) => {
    let [ x1, y1 ] = startPosition;
    let [ x2, y2 ] = endPosition;
    let topLeft     = [min(x1, x2), min(y1, y2)];
    let bottomRight = [max(x1, x2), max(y1, y2)];
    let widthHeight = [bottomRight[0] - topLeft[0], bottomRight[1] - topLeft[1]];

    // NOTE: positive X (right), positive Y (down)
    let vector = subV(endPosition, startPosition);
    let angle = mod(atan2(vector[1], vector[0]), 2*PI);
    let type;
    if (PI * 0.0 <= angle && angle <= PI * 0.5) type = 'down-right';
    if (PI * 0.5 <= angle && angle <= PI * 1.0) type = 'left-down';
    if (PI * 1.0 <= angle && angle <= PI * 1.5) type = 'up-left';
    if (PI * 1.5 <= angle && angle <= PI * 2.0) type = 'right-up';
    if (!clockwise) {
        type = type.split('-').reverse().join('-');
    }
    return (
        <div className={CN(className, 'edge', `edge--${type}`)}
            style={_.extend(cssXY(topLeft), cssWH(widthHeight))}/>
    );
};

const tweakR = 15;
const tweakT = 500;
const tweakPx = (start, end) => {
    let v = subV(end, start);
    let n = normV(v);
    let factor = 1 - Math.exp(-(n / tweakT));
    return mulV(v, factor * tweakR / normV(v));
};

/*
Auto-layout edge for given source/target positions
s: source
t: target
   |   |
 1 | 2 | 3
___s___|___
   |   |
 4 | 5 | 6
___|___t___
   |   |
 7 | 8 | 9
   |   |
(case 1 and 9 is not really well-defined, but it works alright.)
*/
const Edge = ({ label, sourcePosition, targetPosition, interPosition,
                onClick, onDoubleClick, moving, isNew }) => {
    let cw1, cw2;
    if (isBetweenV([sourcePosition, targetPosition], interPosition)) { // case 5
        cw1 = true; cw2 = false;
    } else {
        let [ x1, y1 ] = subV(targetPosition, interPosition);
        let [ x2, y2 ] = subV(interPosition, sourcePosition);
        let angle1 = atan2(y1, x1);
        let angle2 = atan2(y2, x2);
        let diff = mod(angle2 - angle1, 2*PI, -PI);
        if (diff >= 0) {              // case 4, 7, 8
            cw1 = cw2 = true;
        } else {                      // case 2, 3, 6
            cw1 = cw2 = false;
        }
    }
    // NOTE: Tweak edge's position so that the edges sharing the same vertex won't exactly overlap.
    let tweakSource = tweakPx(sourcePosition, interPosition);
    let tweakTarget = tweakPx(interPosition, targetPosition);
    let prop1 = {
        startPosition: plusV(sourcePosition, tweakSource),
        endPosition: interPosition,
        clockwise: cw1,
    };
    let prop2 = {
        startPosition: interPosition,
        endPosition: subV(targetPosition, tweakTarget),
        clockwise: cw2,
    }
    return (
        <div className='edge-container'>
            <EdgePart {...prop1} className={CN({ 'edge--moving': moving, 'edge--new': isNew })} />
            <div className={CN('edge-inter')}
                style={cssXY(interPosition)}
                onClick={onClick} />
            <div className={CN('edge-label')} style={cssXY(interPosition)}>
                <div className='edge-label__inner' onDoubleClick={onDoubleClick}>{ label }</div>
            </div>
            <EdgePart {...prop2} className={CN({ 'edge--moving': moving, 'edge--new': isNew })} />
        </div>
    );
};

export default Edge;
